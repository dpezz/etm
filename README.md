# ETM - landing page - Master
This is a starter template for Elige tu mecanido project

## How to use this template
Required:
* nodejs

```bash
$ git clone https://gitlab.com/dpezz/etm.git
$ git checkout dev // change to branch dev
$ cd etm/
$ npm install
$ gulp
```

start server [http://localhost:4200](http://localhost:4200) 


## Authors
Developed by [Daniel Jara Pezzuoli](http://dpezz.xyz)
For help, please contact the [mail](mailto:jara.pezzuoli@gmail.com)

:-)
